# Blistinator

## Objective
```
Blistinator was created as a means to parse data from business-lists.com as it contains
information that can assist in certain information gathering and reconnaissance activities.
The tool supports both google company name queries and feeding in a list of pre-collected 
business-list formatted urls. The latter is useful if these were identified through previous
search engine queries.
```


## Usage
```
$ python ./blistinator.py -h
Info: Blistinator was created by Hao Nguyen and Chris Patten
Purpose: scrape contact information from business-lists.com, list-brokers.com, connect.data.com and linkedin.com
Contact: neygun[t.a.]gmail.com; @neygun
Contact: cpatten[t.a.]packetresearch.com; @packetassailant

Usage:  python ./blistinator.py -s "company-name"
-h or --help         Print this help menu
-s or --search       Search Google for cached company identifiers
-i or --infile       File containing business-lists formatted urls
-o or --outfile      CSV output file name (Required)
-v or --version      Prints the current version
```

## Installation
```
# Installation
# -----------------------------------------------
# blistinator was tested on Ubuntu 12.04 and OSX Mavericks
# ----------- OSX ---------------
# OSX Deps: pip install -U -r requirements.txt
# ----------- Linux -------------
# Linux: sudo apt-get install python-pip
# Linux Deps: pip install -U -r requirements.txt
# ----------- Required ----------
# Google: Both an CSE API and CX key
#       CX key - https://www.google.com/cse/
#           Add new custom search engine
#           Add to "Sites to search":
#               www.business-lists.com
#               www.list-brokers.com
#               connect.data.com
#       API key - https://console.developers.google.com/project
#           Create Project
#           APIs & auth -> APIs to enable Custom Search
#           APIs & auth -> Credentials -> Create new Key
#           Use Server key
# LinkedIn : Vetted API key and developer token - https://www.linkedin.com/secure/developer
```

## Sample Run
```
$ python blistinator.py -s 'fishnet security' -o test.txt

[+] 8 results found

+-----+----------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------+
|  Id | Company                                                                                | Reference                                                                            |
+-----+----------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------+
| [0] | Fishnet Security - Alpharetta, GA- Computer Related Services, Not Elsewhere Classified | http://www.business-lists.com/search/detail.asp?id=5B1DE68265EA784079D71F6C3E5A87E0  |
| [1] | Fishnet Security - Dublin, OH- Guard services                                          | https://www.business-lists.com/search/detail.asp?id=068690A8B3A09DF2B7FF13210BC4ACED |
| [2] | Fishnet Securities - Indianapolis, IN- Security brokers and dealers                    | http://www.business-lists.com/search/detail.asp?id=1FACD40B54701D8B8AF95DE4B9E9EF41  |
| [3] | Fishnet Security - Kansas City, MO- Computer related consulting services               | http://www.list-brokers.com/search/detail.asp?id=1F75C121C1D819F96C1DF13012451726    |
| [4] | Fishnet Security - Saint Paul, MN- Security Systems Services                           | http://www.list-brokers.com/search/detail.asp?id=B63C9201FFD8A78A028569C2E5207A39    |
| [5] | Fishnet Security - Omaha, NE- Guard services                                           | http://www.list-brokers.com/search/detail.asp?id=05F8D63EEFF7CF4C99864F4332777B7B    |
| [6] | Fishnet Security - Herndon, VA- Engineering services                                   | http://www.list-brokers.com/search/detail.asp?id=C67EF328005376F001211E8D700F1D5F    |
| [7] | Fishnet Security Inc - Addison, TX- Personal document and information services         | http://www.list-brokers.com/search/detail.asp?id=AE30B99CFE0570C94A0ADBDC67B8FD4E    |
| [8] | Enter your own URL                                                                     | Enter your own URL                                                                   |
+-----+----------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------+
Enter multiple URL selections one at a time. Enter your own URL or press <ENTER> to start scraping.
Select a URL to scrape: 4
Select a URL to scrape: 

[+] Scraping 1 URL(s)

[+] 18 employee record(s) found

The default is 1 request at a time.
Enter the number of simultaneous requests, max of 10. <ENTER> for default: 10
Using the value of 10 request(s)...Get a cup of coffee...This can take a minute
+------+-------------------+------------------------------------+---------------------------------------+---------------+--------------+
| Id   | Employee          | Role                               | Email Address                         | Generic Phone | Direct Phone |
+------+-------------------+------------------------------------+---------------------------------------+---------------+--------------+
| [0]  | Scott Feero       | Account Executive                  | scott.feero@fishnetsecurity.com       | 651-225-4478  | 651-291-6320 |
| [1]  | Bob Herman        | Director Network Security          | bob.herman@fishnetsecurity.com        | 651-225-4478  | 816-421-6611 |
| [2]  | Bob Bob Bassett   | Cissp Cisa Cism                    | bob.bassett@fishnetsecurity.com       | 651-225-4478  | 651-291-6318 |
| [3]  | Eric Kraus        | Account Executive                  | eric.kraus@fishnetsecurity.com        | 651-225-4478  | 651-291-6311 |
| [4]  | David Hester      | Account Executive                  | david.hester@fishnetsecurity.com      | 651-225-4478  | 651-291-6313 |
| [5]  | Abu-safieh Hytham | Security Engineer                  | hytham.abu-safieh@fishnetsecurity.com | 651-225-4478  | 816-421-6611 |
| [6]  | Jon Schmidt       | Account Executive                  | jon.schmidt@fishnetsecurity.com       | 651-225-4478  | 651-291-6316 |
| [7]  | Matt Jannusch     | Security Engineer                  | mjannusch@fishnetsecurity.com         | 651-225-4478  | 651-291-6310 |
| [8]  | Mary Larson       | Owner                              | sms@fishnet.com                       | 651-225-4478  | 651-456-0100 |
| [9]  | Rick Okeefe       | Broker                             | rickokeefe@fishnet.com                | 651-225-4478  | 651-365-0230 |
| [10] | Mike Obrien       | Account Manager                    | mike.obrien@fishnetsecurity.com       | 651-225-4478  | Not Found    |
| [11] | Chris Slater      | Account Executive                  | chris.slater@fishnetsecurity.com      | 651-225-4478  | 651-291-6324 |
| [12] | Dan Shea          | Account Executive                  | dan.shea@fishnetsecurity.com          | 651-225-4478  | Not Found    |
| [13] | Tom St Peter      | Sales Manager                      | tom.st.peter@fishnetsecurity.com      | 651-225-4478  | Not Found    |
| [14] | Dan Thormodsgaard | Director Of Solutions Architecture | thor@fishnetsecurity.com              | 651-225-4478  | 651-225-4481 |
| [15] | Gerri Migletz     | Sales Associate                    | gerri-cb-re@fishnet.com               | 651-225-4478  | 651-452-5950 |
| [16] | Dennis Migletz    | Realtor                            | montreux-usa@fishnet.com              | 651-225-4478  | 651-452-5950 |
| [17] | John McGovern     | Realtor                            | jmac@fishnet.com                      | 651-225-4478  | 651-636-9000 |
+------+-------------------+------------------------------------+---------------------------------------+---------------+--------------+
[+] 18 result(s) written to test.txt
Unknown Format: Abu-safieh Hytham - hytham.abu-safieh@fishnetsecurity.com
Unknown Format: Mary Larson - sms@fishnet.com
Unknown Format: Tom St Peter - tom.st.peter@fishnetsecurity.com
Unknown Format: Dan Thormodsgaard - thor@fishnetsecurity.com
Unknown Format: Gerri Migletz - gerri-cb-re@fishnet.com
Unknown Format: Dennis Migletz - montreux-usa@fishnet.com
Unknown Format: John McGovern - jmac@fishnet.com

Determining email format...
+------+-------------------------------------------+-------+
| ID   | Email Format                              | Count |
+------+-------------------------------------------+-------+
| [0]  | <first name><last name>                   | 1     |
| [1]  | <first name>.<last name>                  | 9     |
| [2]  | <first initial>.<last name>               | 0     |
| [3]  | <first initial><last name>                | 1     |
| [4]  | <first name>.<last initial>               | 0     |
| [5]  | <first name>                              | 0     |
| [6]  | <first name>.<middle initial>.<last name> | 0     |
| [7]  | <last name><first initial>                | 0     |
| [8]  | <first name><last initial>                | 0     |
| [9]  | <last name>.<first initial>               | 0     |
| [10] | other                                     | 7     |
+------+-------------------------------------------+-------+
Select the format to use for generating email addresses: 
Invalid choice
Highest count for a valid format is 9
Defaulting to choice 1

Discovered domains:
+-----+----------------------+-------+
| ID  | Domain               | Count |
+-----+----------------------+-------+
| [0] | @fishnetsecurity.com | 13    |
| [1] | @fishnet.com         | 5     |
+-----+----------------------+-------+
Select the domain to use for generating email addresses: 0
Using @fishnetsecurity.com

[+] 2 results found from connect.data.com
+-----+-----------------------------------------------------------------------+------------------------------------------------------------------------------------------------+
|  Id | Company                                                               | Reference                                                                                      |
+-----+-----------------------------------------------------------------------+------------------------------------------------------------------------------------------------+
| [0] | FishNet Security Company Directory of Business Contacts from Data ... | https://connect.data.com/directory/company/list/qPKUuAJ1gS795Kfji7hGfg/fishnet-security        |
| [1] | FishNet Security - Data.com Connect                                   | https://connect.data.com/directory/company/list/qPKUuAJ1gS795Kfji7hGfg/fishnet-security?page=2 |
| [2] | Enter your own URL                                                    | Enter your own URL                                                                             |
+-----+-----------------------------------------------------------------------+------------------------------------------------------------------------------------------------+
Enter multiple URL selections one at a time. Enter your own URL or press <ENTER> to start scraping.
Select a URL to scrape: 0
Select a URL to scrape: 

[+] Scraping 1 URL(s)
[+] 278 result(s) written to test.txt
[+] Scraping LinkedIn...
Only one coId found, using [9291] - FishNet Security
[+] 581 employees found
[-] Discovered employees greater than LinkedIn API throttle of 250
[+] Number of records scraped will be limited due to the throttle
[+] 110 result(s) written to test.txt
```

## Developing
```
Alpha code under active development (version 0.3.2)
```

## Contact
```
# Author: Hao Nguyen
# Contact (Email): neygun[t.a.]gmail[t.o.d]com
# Contact (Twitter): neygun
#
# Author: Chris Patten
# Contact (Email): cpatten[t.a.]packetresearch[t.o.d]com
# Contact (Twitter): packetassailant
```
