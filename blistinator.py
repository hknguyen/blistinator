#!/usr/bin/env python2.7

import sys
import grequests 
import requests
import json
import getopt
from bs4 import BeautifulSoup
from prettytable import PrettyTable
from ConfigParser import SafeConfigParser
import oauth2 as oauth
import httplib2
import time, os, json
import re
import math

VERSION = '0.3.3'

def usage():
    print 'Info: Blistinator was created by Hao Nguyen and Chris Patten'
    print 'Purpose: scrape contact information from business-lists.com, list-brokers.com, connect.data.com and linkedin.com'
    print 'Contact: neygun[t.a.]gmail.com; @neygun'
    print 'Contact: cpatten[t.a.]packetresearch.com; @packetassailant\n'
    print 'Usage:  python ./blistinator.py -s "company-name"'
    print '-h or --help         Print this help menu'
    print '-s or --search       Search Google for cached company identifiers'
    print '-i or --infile       File containing business-lists formatted urls'
    print '-o or --outfile      CSV output file name (Required)'
    print '-v or --version      Prints the current version'

# Instantiates the config parser object
def initConfig():
    configDict = {}  
    parser = SafeConfigParser()
    parser.read('config.ini')

    configDict['cx_key'] = parser.get('google_api', 'cx_key')
    configDict['api_key'] = parser.get('google_api', 'api_key')
    configDict['consumer_key'] = parser.get('oauth_linkedin', 'consumer_key')
    configDict['consumer_secret'] = parser.get('oauth_linkedin', 'consumer_secret')
    configDict['user_token'] = parser.get('oauth_linkedin', 'user_token')
    configDict['user_secret'] = parser.get('oauth_linkedin', 'user_secret')
    return configDict

class LinkinLog:
    def __init__(self, name, outfile, configDict, domain):
        self.comp_name = name
        self.outfile = outfile
        self.consumer_key = configDict['consumer_key']
        self.consumer_secret = configDict['consumer_secret']
        self.user_token = configDict['user_token']
        self.user_secret = configDict['user_secret']
        self.domain = domain[1:len(domain)]
        self.coId = ""
        self.throttle = 250 #API throttle limit

    def oauthSetter(self):  
        # Use your API key and secret to instantiate consumer object
        consumer = oauth.Consumer(self.consumer_key, self.consumer_secret)
         
        # Use the consumer object to initialize the client object
        client = oauth.Client(consumer)
         
        # Use your developer token and secret to instantiate access token object
        access_token = oauth.Token(
            key = self.user_token,
            secret = self.user_secret)
         
        client = oauth.Client(consumer, access_token)
        headers = {'x-li-format':'json'}
        reqDict = {}
        reqDict['client'] = client
        reqDict['headers'] = headers
        peopleList = self.companyIdGetter(reqDict)
        return peopleList

    def companyIdGetter(self, reqDict):
        print "[+] Scraping LinkedIn..."
        coIdList = []
        coIdDict = {}
        # Make call to LinkedIn to retrieve your own profile
        resp,content = reqDict['client'].request("http://api.linkedin.com/v1/companies?email-domain={0}".format(self.domain), "GET", "", reqDict['headers'])

        # this will be used to get the company id used for future requests
        count = 0
        json_entry = json.loads(content)
        if json_entry.has_key('errorCode'):
            print "[-] Error: Status %s - %s" % (json_entry['status'], json_entry['message'])
            sys.exit(0)
        else:
            baselen = len(json_entry['values'])
            while (count < baselen):
                coIdDict['id'] = count
                coIdDict['coId'] = json_entry['values'][count]['id']
                coIdDict['name'] = json_entry['values'][count]['name']
                coIdList.append(coIdDict.copy())
                count += 1  
          
            if len(coIdList) == 1:
                self.coId = [ coIdDict['coId']  ]
                print "Only one coId found, using %s - %s" % (self.coId, coIdDict['name'])
            else:
                theader = PrettyTable([
                    "ID",
                    "coId",
                    "Company"
                    ])
                theader.align["ID"] = "l"
                theader.align["coId"] = "l"
                theader.align["Company"] = "l"
                theader.padding_width = 1  
                
                for item in coIdList:
                    theader.add_row([
                        "[" + str(item['id']) + "]",
                        item['coId'],
                        item['name']
                        ])
                
                print theader
                choice = -1
                while int(choice) >= len(coIdList) or int(choice) < 0:
                    choice = raw_input("Select the coId for scraping linkedin.com: ")
                    try:
                        choice = int(choice)
                        if choice < len(coIdList) and choice >= 0:
                            self.coId = [ c['coId'] for c in coIdList if c['id'] is choice ]
                            print "Using %s" % (self.coId[0])
                    except:
                        print "Invalid choice"
                        choice = -1 
            
            peopleList = self.companyPeopleGetter(reqDict)
            return peopleList

    def companyPeopleGetter(self, reqDict):
        fieldSelectors = "id,first-name,last-name,picture-url,headline,phone-numbers,im-accounts,twitter-accounts"
        # Pre-flight the request and get total returned results
        resp,content = reqDict['client'].request("http://api.linkedin.com/v1/people-search?facet=current-company,{0}".format(self.coId[0]), "GET", "", reqDict['headers'])
        json_entry = json.loads(content)
        if json_entry.has_key('errorCode'):
            print "[-] Error: Status %s - %s" % (json_entry['status'], json_entry['message'])
            sys.exit(0)
        else:
            paginateCeiling = json_entry['people']['_total']
            print "[+] %s employees found" % paginateCeiling
            if paginateCeiling > self.throttle:
                print "[-] Discovered employees greater than LinkedIn API throttle of %s" % self.throttle
                print "[+] Number of records scraped will be limited due to the throttle" 
            count = 0
            peopleList = []
            while (count < paginateCeiling):
                esp,content = reqDict['client'].request("http://api.linkedin.com/v1/people-search:(people:({0}))?facet=current-company,{1}&start={2}".format(fieldSelectors, self.coId[0], count), "GET", "", reqDict['headers'])
                json_entry = json.loads(content)
                if json_entry.has_key('people'):
                    if json_entry['people'].has_key('values'):
                        peopleList  += json_entry['people']['values']
                count += 10
            return peopleList   

    def formatResults(self, peopleList, emailformat):
        records = []
        for person in peopleList:
            record_dict = {}
            fname = person['firstName'].lower()
            lname = person['lastName'].lower()
            record_dict["name_info"]= person['firstName'] + " " + person['lastName']
            if person.has_key('headline'):
                record_dict["title_info"] = person['headline']
            else:
                record_dict["title_info"] = "Not Found"
            record_dict["department_info"] = "Not Found"
            record_dict["level_info"] = "Not Found"
            record_dict["city_info"] = "Not Found"
            record_dict["state_info"] = "Not Found"
            if emailformat[0] == "<first initial><last name>":
                record_dict["email_info"] = fname[0] + lname + "@" + self.domain
            elif emailformat[0] == "<first initial>.<last name>":
                record_dict["email_info"] = fname[0] + '.' + lname + "@" + self.domain
            elif emailformat[0] == "<first name><last name>":
                record_dict["email_info"] = fname + lname + "@" + self.domain
            elif emailformat[0] == "<first name>.<last name>":
                record_dict["email_info"] = fname + '.' + lname + "@" + self.domain
            elif emailformat[0] == "<first name>":
                record_dict["email_info"] = fname + "@" + self.domain
            elif emailformat[0] == "<first name><last initial>":
                record_dict["email_info"] = fname + lname[0] + "@" + self.domain
            elif emailformat[0] == "<first name>.<last initial>":
                record_dict["email_info"] = fname + '.' + lname[0] + "@" + self.domain
            elif emailformat[0] == "<last name><first initial>":
                record_dict["email_info"] = lname + fname[0] + "@" + self.domain
            elif emailformat[0] == "<last name>.<first initial>":
                record_dict["email_info"] = lname + '.' + fname[0] + "@" + self.domain
            else:
                record_dict["email_info"] = "Not Found"
            record_dict["phone_info"] = "Not Found"
            record_dict["direct_phone_info"] = "Not Found"
            records.append(record_dict)

        if self.outfile:
            write_outfile(records, self.outfile, "linkedin.com") 

class Jigdata:
    def __init__(self, name, outfile, configDict):
        self.comp_name = name
        self.glist = []
        self.outfile = outfile
        self.emailformat = ""
        self.domain = ""
        self.cx_key = configDict['cx_key']
        self.api_key = configDict['api_key']
        self.jig_cookie =  ""

    def get_company(self):
        gdict = {}
        idx = 0
        co_id = self.comp_name
        url = "connect.data.com/directory/company/list"
        gurl = ('https://www.googleapis.com/customsearch/v1?q=' + co_id + '&cx=' + \
            self.cx_key + \
            '&siteSearch=' + url + '&key=' + self.api_key \
            )
        headers = { \
        "Host": "www.googleapis.com", \
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0", \
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", \
        "Accept-Language": "en-US,en;q=0.5", \
        "Accept-Encoding": "gzip, deflate", \
        "Connection": "keep-alive"
        }
        gresponse = requests.get(gurl, headers=headers)
        data = json.loads(gresponse.text)
        if data.get('items'):
            for item in data['items']:
                lnk = item['link']
                gdict['count'] = idx
                gdict['link'] = lnk
                gdict['company'] = item['title']
                self.glist.append(gdict.copy())
                idx += 1
        else:
            print "The web search returned no results for %s..." % (co_id)
        self.get_requests()

    def display_company_data(self):
        if len(self.glist) is not None:
            print "\n[+] %d results found from connect.data.com" % (len(self.glist))
            theader = PrettyTable(["Id", "Company", "Reference"])
            theader.align["Count"] = "l"
            theader.align["Company"] = "l"
            theader.align["Reference"] = "l"
            theader.padding_width = 1
            for result in self.glist:
                theader.add_row(["[" + str(result['count']) + "]", result['company'], result['link']])
            theader.add_row(["[" + str(len(self.glist)) + "]", "Enter your own URL", "Enter your own URL"])
            print theader
        else:
            print "\n[+] No results found\n"
    
    def email_format(self, emp_list):
        format_id = 0
        format_counts = []
        choice = -1
        ready = False
        domains = []
        domains_dict = {}
        formats = {"<first initial><last name>": 0,
                "<first initial>.<last name>": 0,
                "<first name><last name>": 0,
                "<first name>.<last name>": 0,
                "<first name>": 0,
                "<first name><last initial>": 0,
                "<first name>.<last initial>": 0,
                "<last name><first initial>": 0,
                "<last name>.<first initial>": 0,
                "<first name>.<middle initial>.<last name>": 0,
                "other": 0}
        results_dict = {}
        results = []
        theader = PrettyTable([
            "ID",
            "Email Format",
            "Count"
            ])
        theader.align["ID"] = "l"
        theader.align["Email Format"] = "l"
        theader.align["Count"] = "l"
        theader.padding_width = 1

        theader_domain = PrettyTable([
            "ID",
            "Domain",
            "Count"
            ])
        theader_domain.align["ID"] = "l"
        theader_domain.align["Domain"] = "l"
        theader_domain.align["Count"] = "l"
        theader_domain.padding_width = 1

        for emp in emp_list:
            name = emp['name_info'].lower()
            space_count = name.count(' ')
            if space_count == 1:
                fname = name[0:name.find(' ')]
                lname = name[name.find(' ')+1:len(name)]
            elif space_count > 1:
                fname = name[0:name.find(' ')]
                lname = name[name.rfind(' ')+1:len(name)]
            
            email = emp['email_info'].lower()
            email_name = email[0:email.find('@')]
            period_count = email_name.count('.')
            domain = email[email.find('@'):len(email)]
            domains.append(domain)

            period_pos = email_name.find('.')
            fname_pos = email_name.find(fname)
            lname_pos = email_name.find(lname)

            if period_pos == -1 and lname_pos == 1 and (email_name.find(fname[0]) == 0) and fname_pos == -1: 
                formats['<first initial><last name>'] += 1
                #print "<first initial><last name>: " + emp['name_info'] + " - " + emp['email_info']
            elif period_pos == 1 and lname_pos == 2 and email_name.find(fname[0]) == 0 and fname_pos == -1:
                formats['<first initial>.<last name>'] += 1
                #print "<first initial.><last name>: " + emp['name_info'] + " - " + emp['email_info']
            elif period_pos == -1 and lname_pos > 1 and fname_pos == 0 and (len(fname) + len(lname)) == len(email_name): 
                formats['<first name><last name>'] += 1
                #print "<first name><last name>: " + emp['name_info'] + " - " + emp['email_info']
            elif period_pos == (lname_pos - 1) and lname_pos > 1 and fname_pos == 0: 
                formats['<first name>.<last name>'] += 1
                #print "<first name>.<last name>: " + emp['name_info'] + " - " + emp['email_info']
            elif fname == email_name:
                formats['<first name>'] += 1
                #print "<first name>: " + emp['name_info'] + " - " + emp['email_info']
            elif period_pos == -1 and email_name.rfind(lname[0]) == (len(email_name) - 1) and fname_pos == 0 and lname_pos == -1:
                formats['<first name><last initial>'] += 1
                #print "<first name><last initial>: " + emp['name_info'] + " - " + emp['email_info']
            elif period_pos == len(fname) and email_name.rfind(lname[0]) == (len(email_name) - 1) and fname_pos == 0 and lname_pos == -1:
                formats['<first name>.<last initial>'] += 1
                #print "<first name>.<last initial>: " + emp['name_info'] + " - " + emp['email_info']
            elif period_pos == -1 and lname_pos == 0 and email_name.rfind(fname[0]) == (len(email_name) - 1) and fname_pos == -1:
                formats['<last name><first initial>'] += 1
                #print "<last name><first initial>: " + emp['name_info'] + " - " + emp['email_info']
            elif period_pos == len(lname) and lname_pos == 0 and email_name.rfind(fname[0]) == (len(email_name) - 1) and fname_pos == -1:
                formats['<last name>.<first initial>'] += 1
                #print "<last name>.<first initial>: " + emp['name_info'] + " - " + emp['email_info']
            elif period_count == 2 and fname_pos == 0 and lname_pos == (email_name.rfind('.') + 1) and (email_name.rfind('.') == email.find('.') + 2):
                formats['<first name>.<middle initial>.<last name>'] += 1
            else:
                formats['other'] += 1
                print "Unknown Format: " + emp['name_info'] + " - " + emp['email_info']

        for key, value in formats.items():
            results_dict['format_id'] = format_id
            results_dict['format'] = key
            results_dict['num'] = value
            results.append(results_dict.copy())
            
            theader.add_row([
                "[" + str(format_id) + "]",
                key,
                value
                ])
            format_counts.append(value)
            format_id += 1
        
        print "\nDetermining email format..."
        print theader
        
        for c in reversed(sorted(format_counts)):
            pick = [ result['format_id'] for result in results if result['num'] is c ]
            invalid_format = [ r['format'] for r in results if r['format_id'] is pick[0] ]
            if invalid_format[0] != "<other>" and invalid_format[0] != "<first name>.<middle initial>.<last name>":
                valid_pick_count = c
                break
        default_choice = [ result['format_id'] for result in results if result['num'] is valid_pick_count ]

        while not ready:
            try:
                choice = int(raw_input("Select the format to use for generating email addresses: "))
                if choice > len(results_dict) or int(choice) < 0:
                    print "Invalid choice"
                    print "Highest count for a valid format is " + str(valid_pick_count)
                    choice = default_choice[0]
                    print "Defaulting to choice %s" % choice
                    ready = True
                else:
                    ready = True
            except:
                print "Invalid choice"
                print "Highest count for a valid format is " + str(valid_pick_count)
                choice = default_choice[0]
                print "Defaulting to choice %s" % choice
                ready = True

        self.emailformat = [ r['format'] for r in results if r['format_id'] is choice ]
        
        if len(set(domains)) == 1:
            self.domain = domain
            print "Only one domain found, using %s for generating email addresses" % (self.domain)
        else:
            print "\nDiscovered domains:"
            temp_domains = domains
            for i, d in enumerate(set(domains)):
                domains_dict[i] = d
                domains_dict['count'] = temp_domains.count(d)
                theader_domain.add_row([
                    "[" + str(i) + "]",
                    d,
                    domains_dict['count']
                    ])

            print theader_domain
            dom_choice = -1
            while int(dom_choice) >= len(domains_dict) or int(dom_choice) < 0:
                dom_choice = raw_input("Select the domain to use for generating email addresses: ")
                try:
                    dom_choice = int(dom_choice)
                    if dom_choice < len(domains_dict) and dom_choice >= 0:
                        self.domain = domains_dict[dom_choice] 
                        print "Using %s" % (self.domain)
                except:
                    print "Invalid choice"
                    dom_choice = -1

    def get_requests(self):
        self.display_company_data()
        choice = -1
        choices = set([])
        tmp_list = []
        ready = False
        print "Enter multiple URL selections one at a time. Enter your own URL or press <ENTER> to start scraping."
        while not ready:
            try:
                choice = int(raw_input("Select a URL to scrape: "))
                if choice > (len(self.glist)) or choice < 0:
                    print "Invalid selection. Value has to be between %d and %d" % (0, len(self.glist))
                elif int(choice) == len(self.glist):
                    entered_url = raw_input("Enter a connect.data.com URL: ")
                    self.glist.append({'count': choice, 'company': u'Enter your own URL', 'link': entered_url})
                    choices.add(choice)
                    ready = True
                else:
                    choices.add(choice)
            except ValueError:
                if len(choices) == 0:
                    print "No URLs selected yet."
                else:
                    ready = True
        print "%s%d%s" % ("\n[+] Scraping ", len(choices)," URL(s)")
        
        for c in choices:
            targeturl = [ result['link'] for result in self.glist if result['count'] is c ]
            param_location = targeturl[0].find("?page")
            if param_location == -1:
                url = targeturl[0]
            else:
                url = targeturl[0][0:param_location]
            self.get_cookie(url)
    
    def get_cookie(self, url):
        headers = { \
               'Host': 'connect.data.com', \
               'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:32.0) Gecko/20100101 Firefox/32.0', \
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', \
               'Accept-Language': 'en-US,en;q=0.5', \
               'Accept-Encoding': 'gzip, deflate', \
               'Referer': url, \
               'Connection': 'keep-alive'
               }
        page = requests.get(url, headers=headers)
        html = BeautifulSoup(page.text)
        item = html.find(text=re.compile("Challenge="))
        chall = item.find("Challenge=")
        challId = item.find("ChallengeId=")
        genericErr = item.find("GenericError")
        
        #obtain challenge and challengeId  
        challengeId = item[challId+len("ChallengeId="):genericErr-3]
        challenge = int(item[chall+len("Challenge="):challId-3])
        var_arr = str(challenge)
        
        #calculate challenge result
        lastDig = int(var_arr[-1])
        minDig = int(min(var_arr))
        var_arr = sorted(reversed(str(challenge)))
        subvar1 = (2 * (int(var_arr[2])))+(int(var_arr[1])*1)
        subvar2 = int(str(2 * int(var_arr[2]))+(var_arr[1]))
        my_pow=math.pow(((int(var_arr[0])*1)+2),int(var_arr[1]))
        x=(challenge*3+subvar1)*1
        y=int(math.cos(math.pi*subvar2))
        answer=x*y
        answer-=my_pow*1
        answer+=(minDig-lastDig)
        answer=str(answer).split('.')[0]+str(subvar2)

        headers['X-AA-Challenge-ID'] = challengeId
        headers['X-AA-Challenge-Result'] = answer
        headers['X-AA-Challenge'] = challenge

        print "[+] Received X-AA-Challenge: %s with X-AA-Challenge-ID: %s " % (headers['X-AA-Challenge'], headers['X-AA-Challenge-ID'])
        print "[+] Calculated X-AA-Challenge-Result: %s" % headers['X-AA-Challenge-Result']

        s = requests.Session()
        r = s.post(url, headers=headers)
        print "[+] Using X-AA-Cookie-Value: %s" % r.headers['x-aa-cookie-value']
        self.jig_cookie =  r.headers['x-aa-cookie-value']
        self.scrape_connect(url)

    def scrape_connect(self, targeturl):
        headers = { \
        'Host': 'connect.data.com', \
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0', \
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', \
        'Accept-Language': 'en-US,en;q=0.5', \
        'Accept-Encoding': 'gzip, deflate', \
        'Referer': targeturl, \
        'Cookie': self.jig_cookie, \
        'Connection': 'keep-alive'
        }
        records = [] 
        urls = []
        page = requests.get(targeturl, headers=headers)
        soup = BeautifulSoup(page.text)
        
        try:
            last_url = soup.find(rel="last")
            last_page = int(last_url['href'][6:])
        except:
            last_page=1
        
        for i in range(1,last_page+1):
            url = targeturl+"?page=%s" % (i)
            urls.append(url)
            
        rs = (grequests.get(u, headers=headers) for u in urls)
        results = grequests.map(rs)
        
        for r in results:
            soup = BeautifulSoup(r.text)
            for row in soup.findAll("tr"):
                emp = []
                record_dict = {}
                EMPTY_RECORD = True
                for td in row.findAll("td"):
                    try:
                        div = td["class"]
                    except:
                        div = [u'None']
                    if div[0] == 'ellipsis':
                        emp.append(td.span.string.encode('ascii', 'ignore').decode('ascii'))
                        EMPTY_RECORD = False
                    elif div[0] == 'None':
                        emp.append(td.string)
                        EMPTY_RECORD = False
                if not EMPTY_RECORD:
                    lname = emp[0].lower()
                    fname = emp[1].lower()
                    record_dict["name_info"] = emp[1] + " " + emp[0]
                    record_dict["title_info"] = emp[2]
                    record_dict["department_info"] = emp[3]
                    record_dict["level_info"] = emp[4]
                    record_dict["city_info"] = emp[5]
                    record_dict["state_info"] = emp[6]
                    if self.emailformat[0] == "<first initial><last name>":
                        record_dict["email_info"] = fname[0] + lname + self.domain
                    elif self.emailformat[0] == "<first initial>.<last name>":
                        record_dict["email_info"] = fname[0] + '.' + lname + self.domain
                    elif self.emailformat[0] == "<first name><last name>":
                        record_dict["email_info"] = fname + lname + self.domain
                    elif self.emailformat[0] == "<first name>.<last name>":
                        record_dict["email_info"] = fname + '.' + lname + self.domain
                    elif self.emailformat[0] == "<first name>":
                        record_dict["email_info"] = fname + self.domain
                    elif self.emailformat[0] == "<first name><last initial>":
                        record_dict["email_info"] = fname + lname[0] + self.domain
                    elif self.emailformat[0] == "<first name>.<last initial>":
                        record_dict["email_info"] = fname + '.' + lname[0] + self.domain
                    elif self.emailformat[0] == "<last name><first initial>":
                        record_dict["email_info"] = lname + fname[0] + self.domain
                    elif self.emailformat[0] == "<last name>.<first initial>":
                        record_dict["email_info"] = lname + '.' + fname[0] + self.domain
                    else:
                        record_dict["email_info"] = "Not Found"
                    record_dict["phone_info"] = "Not Found"
                    record_dict["direct_phone_info"] = "Not Found"
                    records.append(record_dict)

        if self.outfile:
            write_outfile(records, self.outfile, "connect.data.com") 

def get_company(name, configDict):
    glist = []
    gdict = {}
    idx = 0
    co_id = name
    cids = []
    urls = ["business-lists.com","list-brokers.com"]
    for url in urls:
        gurl = ("https://www.googleapis.com/customsearch/v1?q=" + co_id + "&cx=" + \
            configDict['cx_key'] + \
            "&siteSearch=" + url + "%2Fsearch%2Fdetail.asp&key=" + configDict['api_key'] \
            )
        headers = { \
        "Host": "www.googleapis.com", \
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:30.0) Gecko/20100101 Firefox/30.0", \
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", \
        "Accept-Language": "en-US,en;q=0.5", \
        "Accept-Encoding": "gzip, deflate", \
        "Connection": "keep-alive"
        }
        gresponse = requests.get(gurl, headers=headers)
        data = json.loads(gresponse.text)
        if data.get('items'):
            for item in data['items']:
                lnk = item['link']
                for x in item['pagemap']['metatags']:
                    gdict['count'] = idx
                    gdict['link'] = lnk
                    gdict['company'] = x['og:title']
                    if lnk[lnk.find("=")+1:len(lnk)] not in cids:
                        glist.append(gdict.copy())
                        idx += 1
                cids.append(lnk[lnk.find("=")+1:len(lnk)])
        else:
            print "The web search returned no results for %s..." % (url)
    return glist    

# send simultaneous requests
def get_blist_request(urls):
    rs = (grequests.get(u) for u in urls)
    results = grequests.map(rs)
    emp_records = []
    for r in results:
       emp_records.append(employee(r))
    return emp_records

def get_num_employees(page, url):
    employees = []
    soup = BeautifulSoup(page.text)
    for row in soup.find_all(href=re.compile("detail2")):
        employees.append(url+row['href'])
    return employees

def set_concurrency(employees):
    emp_list = []
    concurrent = -1
    print "\n[+] %d employee record(s) found\n" % (len(employees))
    print "The default is 1 request at a time."
    while int(concurrent) > 10 or int(concurrent) <= 0:
        concurrent = raw_input("Enter the number of simultaneous requests, max of 10. <ENTER> for default: ")
        try:            
            concurrent = int(concurrent)
            if concurrent <= 10 and concurrent > 0:
                print "%s%d%s" % ("Using the value of ", concurrent, " request(s)...Get a cup of coffee...This can take a minute")
        except:
            print "Using default value of 1 request...Get a cup of coffee...This can take a minute"
            concurrent = 1
    for i in range(0, len(employees), concurrent):
        record = get_blist_request(employees[i:i+concurrent])
        emp_list.extend(record)
    return emp_list

def get_multi_url_request(infile, outfile=None):
    count = 0
    tmp_list = []
    theader = PrettyTable(["Id", "Target URL"])
    theader.align["Id"] = "l"
    theader.align["Target URL"] = "l"
    theader.padding_width = 1
    for targeturl in infile:
        theader.add_row([
            "[" + str(count) + "]", 
            targeturl.strip()
            ]) 
        page = requests.get(targeturl)
        url = targeturl.find("detail.asp")
        tmp_list.append(get_num_employees(page, targeturl[0:url]))
        new_list = list(set(reduce(lambda x,y: x+y,tmp_list)))
        count += 1
    print theader
    print "%s%d%s" % ("[+] ", count," URLs loaded for testing")
    emp_list = set_concurrency(new_list)
    display_emp_data(emp_list)
    if outfile:
        write_outfile(emp_list, outfile, "business-lists.com")
    return emp_list

def write_outfile(emp_list, filename, source):
    try:
        count = 0
        fo = open(filename, "a")
        if fo.tell() == 0:
            fo.write("Id;Employee;Role;Email Address;Generic Phone;Direct Phone;Department;Level;City;State;Source\n")
        else:
            count = sum(1 for line in open(filename)) - 1
        for r in emp_list:
            fo.write("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s" % (str(count),r['name_info'],r['title_info'],r['email_info'],r['phone_info'],r['direct_phone_info'],r['department_info'],r['level_info'],r['city_info'],r['state_info'],source,"\n"))
            count += 1
        fo.close()
        print "[+] %d result(s) written to %s" % (len(emp_list), filename)
    except IOError:
        print "[-] File error, printing results..."

def display_emp_data(emp_list):
    count = 0
    theader = PrettyTable([
        "Id", 
        "Employee", 
        "Role",
        "Email Address",
        "Generic Phone",
        "Direct Phone"
        ])
    theader.align["Id"] = "l"
    theader.align["Employee"] = "l"
    theader.align["Role"] = "l"
    theader.align["Email Address"] = "l"
    theader.align["Generic Phone"] = "l"
    theader.align["Direct Phone"] = "l"
    theader.padding_width = 1
    for result in emp_list:
        theader.add_row([
            "[" + str(count) + "]", 
            result['name_info'], 
            result['title_info'],
            result['email_info'],
            result['phone_info'],
            result['direct_phone_info']
            ])
        count += 1
    try:
        print theader
    except RuntimeError:
        print "Error printing employee data, will save to file."

def display_company_data(glist):
    if len(glist) is not None:
        print "\n[+] %d results found\n" % (len(glist))
        theader = PrettyTable(["Id", "Company", "Reference"])
        theader.align["Count"] = "l"
        theader.align["Company"] = "l"
        theader.align["Reference"] = "l"
        theader.padding_width = 1
        for result in glist:
            theader.add_row(["[" + str(result['count']) + "]", result['company'], result['link']])
        theader.add_row(["[" + str(len(glist)) + "]", "Enter your own URL", "Enter your own URL"])
        print theader
    else:
        print "\n[+] No results found\n"

# make both the google and blist request / process results
def get_master_request(glist, outfile=None):
    display_company_data(glist)
    choice = -1
    choices = set([])
    tmp_list = []
    ready = False
    print "Enter multiple URL selections one at a time. Enter your own URL or press <ENTER> to start scraping."
    while not ready:
        try:
            choice = int(raw_input("Select a URL to scrape: "))
            if choice > (len(glist)) or choice < 0:
                print "Invalid selection. Value has to be between %d and %d" % (0, len(glist))
            elif int(choice) == len(glist):
                entered_url = raw_input("Enter a business-lists.com URL: ")
                glist.append({'count': choice, 'company': u'Enter your own URL', 'link': entered_url})
                choices.add(choice)
                ready = True
            else:
                choices.add(choice)
        except ValueError:
            if len(choices) == 0:
                print "No URLs selected yet."
            else:
                ready = True
        
    for c in choices:
        targeturl = [ result['link'] for result in glist if result['count'] is c ]
        page = requests.get(targeturl[0])
        url = targeturl[0].find("detail.asp")
        tmp_list.append(get_num_employees(page, targeturl[0][0:url]))
        new_list = list(set(reduce(lambda x,y: x+y,tmp_list)))
    
    print "%s%d%s" % ("\n[+] Scraping ", len(choices)," URL(s)")
    emp_list = set_concurrency(new_list)
    display_emp_data(emp_list)
    if outfile:
        write_outfile(emp_list, outfile, "business-lists.com")
    return emp_list

# check if contact item exists
def check_contact_info(item):
    if item is None:
        return "Not found"
    else:
        return item.string

# extract contact info for each employee record
def employee(emp_page):
    emp_dict = {}
    soup = BeautifulSoup(emp_page.text)
    record = soup.find("h3") #list-brokers.com
    if record is None:
        record = soup.find("strong") #business-lists.com
    name = soup.find(itemprop="name")
    title = soup.find(itemprop="title")
    phone = soup.find(itemprop="telephone")
    email = soup.find(itemprop="email")
    name_info = check_contact_info(name)
    title_info = check_contact_info(title)
    phone_info = check_contact_info(phone)
    email_info = check_contact_info(email)
    try:
        contact_info = str(record.br.contents[1])
        direct_phone_location = contact_info.find("Direct Phone:")
        if direct_phone_location != -1:
            direct_phone_info = contact_info[direct_phone_location+14:direct_phone_location+26]
        else:
            direct_phone_info = "Not Found"
    except IndexError:
        print "[-] Error finding direct phone information for %s" % (name)
        print "Try running the tool again if few results were found"
        direct_phone_info = "Not Found"
    emp_dict['name_info'] = name_info
    emp_dict['title_info'] = title_info
    emp_dict['phone_info'] = phone_info
    emp_dict['direct_phone_info'] = direct_phone_info
    emp_dict['email_info'] = email_info
    emp_dict['department_info'] = "Not Found"
    emp_dict['level_info'] = "Not Found"
    emp_dict['city_info'] = "Not Found"
    emp_dict['state_info'] = "Not Found"
    return emp_dict

def main():
    found_s = False
    found_i = False
    found_o = False

    try:
        (opts, args) = getopt.getopt(sys.argv[1:], 'hvs:i:o:', ['infile=',
            'outfile=', 'search='])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    if len(opts) == 0:
        usage()
        sys.exit(2)
    for (opt, arg) in opts:
        if opt in ('-h', '--help'):
            usage()
            sys.exit(2)
        elif opt in ('-i', '--infile'):
            found_i = True
            infile = open(arg, 'r')
        elif opt in ('-o', '--outfile'):
            found_o = True
            outfile = arg
        elif opt in ('-s', '--search' ):
            found_s = True
            name = arg
        elif opt in ('-v', '--version'):
            print "%s%s" % ("Current version: ", VERSION)
            sys.exit(2)
        else:
            assert False, 'unhandled option'
    if not found_s and not found_i:
        print  '-s or -i are mandatory arguments'
        usage()
        sys.exit(2)
    elif found_s and found_i:
        print  '-s and -i are mutually exclusive'
        usage()
        sys.exit(2)
    elif not found_o:
        print '-o is a mandatory argument'
        usage()
        sys.exit(2)
    elif found_s and not found_i:
        configDict = initConfig()
        glist = get_company(name, configDict)
        jig_scrape = Jigdata(name, outfile, configDict)
        blist_data = get_master_request(glist, outfile)
        jig_scrape.email_format(blist_data)
        jig_scrape.get_company()
        linkin_scrape = LinkinLog(name, outfile, configDict, jig_scrape.domain)
        peopleList = linkin_scrape.oauthSetter()
        linkin_scrape.formatResults(peopleList, jig_scrape.emailformat)
    elif found_i and not found_s:
        configDict = initConfig()
        blist_data = get_multi_url_request(infile, outfile)
        name = raw_input("Enter the company to search for connect.data.com page: ")
        jig_scrape = Jigdata(name, outfile, configDict)
        jig_scrape.email_format(blist_data)
        jig_scrape.get_company()
        linkin_scrape = LinkinLog(name, outfile, configDict, jig_scrape.domain)
        peopleList = linkin_scrape.oauthSetter()
        linkin_scrape.formatResults(peopleList, jig_scrape.emailformat)
    else:
        print "Unknown Option"
        usage()
        sys.exit(2)
 
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print "Caught KeyboardInterrupt, exiting..."
        sys.exit(0)
